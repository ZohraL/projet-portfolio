 <H1> Projet Portfolio </h1>
Bienvenue sur l'étape 1 du portfolio ! 



<strong> Contact </strong>

Zohra Laïd

 laid.zohra@gmail.com

Lien [LinkedIn](https://www.linkedin.com/feed/?trk=hb_signin)

Lien [GitLab](https://gitlab.com/ZohraL/projet-portfolio)



<strong>User Stories </strong>

* En tant qu'étudiante, je souhaite regarder les portfolios précédents afin de m'orienter dans mon projet. 

* En tant que professeur, je souhaite suivre les progressions des élèves afin de connaitre l'avancée de leurs projets. 

* En tant que recruteur, je souhaite avoir accès aux portfolios des élèves de Simplon afin de dénicher une pépite. 
* En tant que recruteur, je souhaite regarder le portfolio d'un candidat afin de vérifier ses compétences. 

<strong>Maquettes fonctionnelles</strong>

https://marvelapp.com/c8b7ae7 

